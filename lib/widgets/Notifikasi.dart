import 'package:flutter/material.dart';

class NotifikasiPage extends StatelessWidget {
  final List<NotificationItem> notifications = [
    NotificationItem(
      icon: AssetImage('assets/images/btc.png'),
      title: 'Bitcoin (BTC)',
      description:
          'Harga Bitcoin mencapai level tertinggi baru sepanjang masa!',
      time: '10:30 AM',
    ),
    NotificationItem(
      icon: AssetImage('assets/images/eth.png'),
      title: 'Ethereum (ETH)',
      description:
          'Pembaruan jaringan Ethereum dijadwalkan untuk minggu depan.',
      time: '09:15 AM',
    ),
    NotificationItem(
      icon: AssetImage('assets/images/usd.png'),
      title: 'Tether (USD)',
      description: 'Kapitalisasi pasar Tether melampaui 100 miliar.',
      time: '08:45 AM',
    ),
    NotificationItem(
      icon: AssetImage('assets/images/ada.png'),
      title: 'Cardano (ADA)',
      description:
          'Cardano mengumumkan kemitraan dengan perusahaan teknologi besar.',
      time: '08:00 AM',
    ),
    NotificationItem(
      icon: AssetImage('assets/images/xrp.png'),
      title: 'Ripple (XRP)',
      description: 'Ripple akan meluncurkan solusi pembayaran baru.',
      time: '07:30 AM',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Crypto Notifikasi'),
        backgroundColor: Colors.purpleAccent,
      ),
      body: ListView.builder(
        itemCount: notifications.length,
        itemBuilder: (context, index) {
          return Card(
            elevation: 2.0,
            child: ListTile(
              leading: Image(image: notifications[index].icon),
              title: Text(notifications[index].title),
              subtitle: Text(notifications[index].description),
              trailing: Text(notifications[index].time),
              onTap: () {
                // TODO: Implement notification details screen
              },
            ),
          );
        },
      ),
    );
  }
}

class NotificationItem {
  final AssetImage icon;
  final String title;
  final String description;
  final String time;

  NotificationItem({
    required this.icon,
    required this.title,
    required this.description,
    required this.time,
  });
}
