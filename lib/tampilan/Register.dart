import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'LoginPage.dart';
import 'homepage.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController passwordValController = TextEditingController();

  bool errorValidator = false;

  Future signUp() async {
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: emailController.text,
        password: passwordValController.text,
      );
      await FirebaseAuth.instance.signOut();
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        print('The account already exists for that email.');
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Halaman Daftar"),
        backgroundColor: Color(0xff8300FF),
      ),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Email",
              ),
              SizedBox(
                height: 10.0,
              ),
              TextField(
                controller: emailController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Tuliskan Email",
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "Password",
              ),
              SizedBox(
                height: 10.0,
              ),
              TextField(
                controller: passwordController,
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Tuliskan Password",
                  errorText: errorValidator ? "Password tidak sama!" : null,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                "Validasi Password",
              ),
              SizedBox(
                height: 10.0,
              ),
              TextField(
                controller: passwordValController,
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: "Tuliskan Password",
                  errorText: errorValidator ? "Password tidak sama!" : null,
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              SizedBox(
                width: double.infinity,
                height: 50.0,
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      passwordController.text == passwordValController.text
                          ? errorValidator = false
                          : errorValidator = true;
                    });
                    if (errorValidator) {
                      print("Error");
                    } else {
                      signUp();
                      Navigator.pop(context);
                    }
                  },
                  child: Text(
                    "Daftar",
                    style: TextStyle(fontSize: 18.0, color: Colors.white),
                  ),
                  style: ElevatedButton.styleFrom(primary: Color(0xff8300FF)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
