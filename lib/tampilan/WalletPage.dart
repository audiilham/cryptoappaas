import 'package:flutter/material.dart';

class WalletPage extends StatefulWidget {
  @override
  _WalletPageState createState() => _WalletPageState();
}

class _WalletPageState extends State<WalletPage> {
  List<String> cryptoAssets = [
    'Bitcoin (BTC)',
    'Ethereum (ETH)',
    'Tether (USDT)',
    'Cardano (ADA)',
    'Ripple (XRP)',
  ];

  List<NotificationItem> notifications = [
    NotificationItem(
      icon: AssetImage('assets/images/btc.png'),
      title: 'Bitcoin (BTC)',
      description: 'Bitcoin price reached a new all-time high!',
      time: '10:30 AM',
    ),
    NotificationItem(
      icon: AssetImage('assets/images/eth.png'),
      title: 'Ethereum (ETH)',
      description: 'Ethereum network upgrade scheduled for next week.',
      time: '09:15 AM',
    ),
    NotificationItem(
      icon: AssetImage('assets/images/usd.png'),
      title: 'Tether (USD)',
      description: 'Tether market cap surpasses 100 billion.',
      time: '08:45 AM',
    ),
    NotificationItem(
      icon: AssetImage('assets/images/ada.png'),
      title: 'Cardano (ADA)',
      description: 'Cardano announces partnership with major tech company.',
      time: '08:00 AM',
    ),
    NotificationItem(
      icon: AssetImage('assets/images/xrp.png'),
      title: 'Ripple (XRP)',
      description: 'Ripple to launch new payment solution.',
      time: '07:30 AM',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Wallet'),
        backgroundColor: Colors.purpleAccent,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.grey[300],
                borderRadius: BorderRadius.circular(4.0),
              ),
              padding: EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Text(
                      'Total Investasi',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(height: 8),
                  Center(
                    child: Text(
                      '\$15,089,000',
                      style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 16),
            Text(
              'Crypto Assets',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 8),
            Expanded(
              child: ListView.builder(
                itemCount: cryptoAssets.length,
                itemBuilder: (context, index) {
                  return Card(
                    elevation: 2.0,
                    child: ListTile(
                      leading: Image(image: notifications[index].icon),
                      title: Text(cryptoAssets[index]),
                      subtitle: Text('Balance: 0.0'),
                      onTap: () {
                        showDialog(
                          context: context,
                          builder: (context) => AlertDialog(
                            title: Text('Detail Assets'),
                            content: Text(
                                'detail assets atau riwayat transaksi Anda disini.'),
                            actions: [
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.pop(context); // Close the dialog
                                },
                                child: Text(
                                  'Close',
                                ),
                                style: ElevatedButton.styleFrom(
                                    primary: Colors.purpleAccent),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NotificationItem {
  final AssetImage icon;
  final String title;
  final String description;
  final String time;

  NotificationItem({
    required this.icon,
    required this.title,
    required this.description,
    required this.time,
  });
}
